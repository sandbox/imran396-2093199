<?php
 drupal_add_css(path_to_theme() . '/css/members.css');
 global $user;
 global $base_url;
 ?>

<div class="memberheader">
<div class="totaluser"><b><?php echo 'All Members ('.$totalUser.')'; ?></b></div>
<div class="search_button">
<form action=<?php echo "$base_url/members/";?> method="get">
<input type="text" name="usersearch" placeholder="Search">
<button type="submit" style="border: 0; background: transparent">
<img src="<?php echo $base_url.'/'.path_to_theme();?>/images/search_icon.png" alt="submit" />
</button>
</form>
</div>
<div class="clear"></div>
</div>

<?php
  foreach($data as $val):
      $userobj = user_load($val->uid);
?>

<div class="MDW300" >
<?php

    if(!empty($userobj->picture->uri)){
           $imagepath = image_style_url('thumbnail',$userobj->picture->uri);
    }else{
          $imagepath = $base_url.'/'.path_to_theme().'/images/default_user.jpg';
    }

 $link = drupal_get_path_alias('user/' . $val->uid);
  ?>
<div class="userImages"><a href="<?php echo $base_url.'/'.$link;?>" ><img src="<?php echo $imagepath;?>" alt="" title = '<?php echo "$userobj->name" ?>'; /></a></div>

</div>

<?php
endforeach;

echo theme('pager');
?>

